Spring Boot applications with following demo projects.

clone/download exmaple projects from respective branch

1) Spring Boot REST API with H2 in memory DB

   <b>Description</b>:- CRUD REST API with Spring boot and H2 In memory DB <br>
   <b>Branch name</b>:- spring-boot-rest-in-memory-db <br>
   <b>Tech In-Use</b>:- Spring boot - 2.0.5.RELEASE <br>
                        Gradle 4.4.1,  <br>
                        RESP API, <br>
                        Spring Data. <br>
                        H2 In-memory DB
